package com.anahata.metamodel;

import com.anahata.util.metamodel.MetaModel;
import com.anahata.util.metamodel.MetaModelProperty;
import com.sun.codemodel.*;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.classworlds.realm.ClassRealm;

/**
 * Generate metamodel classes.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 * @author Pablo Rodriguez <pablo@anahata-it.com.au>
 */
@Slf4j
@Mojo(name = "process", defaultPhase = LifecyclePhase.GENERATE_SOURCES,
        requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class MetaModelGenerator extends AbstractMojo {
    /**
     * List of classes to generate a metamodel for.
     */
    @Parameter(required = true)
    private List<String> classes;

    @Parameter(defaultValue = "${project.build.directory}/generated-sources/metamodel")
    private String outputDirectory;

    @Component
    private MavenProject project;

    @Override
    public void execute() throws MojoExecutionException {
        if (classes == null || classes.isEmpty()) {
            throw new MojoExecutionException("No classes were listed, at least one is required");
        }

        //Copied from https://code.google.com/p/eclipselink-staticweave-maven-plugin/source/browse/trunk/src/main/java/au/com/alderaan/eclipselink/mojo/EclipselinkStaticWeaveMojo.java
        ClassRealm c;
        try {
            // Thread context class loader is the ClassRealm for this plugin.
            c = (ClassRealm)Thread.currentThread().getContextClassLoader();
            // Add Project output directory to class path.
            c.addURL(new File(project.getBuild().getOutputDirectory()).toURI().toURL());
            // Add Project class path to class path.
            for (URL url : buildClassPath()) {
                c.addURL(url);
            }
        } catch (Exception e) {
            throw new MojoExecutionException("Exception building classpath", e);
        }

        JCodeModel jcm = new JCodeModel();
        List<Class> sourceClasses = new ArrayList<>();
        List<JDefinedClass> definedClasses = new ArrayList<>();

        for (String className : classes) {
            Class clazz;

            try {
                clazz = Class.forName(className);
                sourceClasses.add(clazz);
            } catch (ClassNotFoundException e) {
                throw new MojoExecutionException("Could not reference class: " + className, e);
            }

            try {
                definedClasses.add(createClass(jcm, clazz));
            } catch (JClassAlreadyExistsException e) {
                throw new MojoExecutionException("Class defined twice: " + className, e);
            }
        }

        // For each generated class, try to find a child class that has been generated and set it as a property
        // so it can be referenced from the top level class.
        for (JDefinedClass definedClass : definedClasses) {
            for (JFieldVar var : definedClass.fields().values()) {
                if (var.type() instanceof JDefinedClass) {
                    JDefinedClass jdclass = (JDefinedClass)var.type();
                    String className = jdclass._package().name() + "." + jdclass.name();
                }
            }
        }

        try {
            File file = new File(outputDirectory);
            file.mkdirs();
            jcm.build(file, file);
            project.addCompileSourceRoot(outputDirectory);
        } catch (IOException e) {
            throw new MojoExecutionException("Error creating output files", e);
        }
    }

    private JDefinedClass createClass(JCodeModel jcm, Class clazz) throws JClassAlreadyExistsException {
        final JDefinedClass jdc = jcm._class(clazz.getName() + "_mm");

        for (PropertyDescriptor pd : PropertyUtils.getPropertyDescriptors(clazz)) {
            if (!pd.getName().equals("class")) {
                createProperty(jcm, jdc, clazz, pd);
            }
        }

        jdc._extends(jcm.ref(MetaModel.class));
        jdc.field(JMod.PUBLIC | JMod.STATIC | JMod.FINAL, jdc, "INSTANCE", JExpr._new(jdc));
        JMethod constructor = jdc.constructor(JMod.PUBLIC);
        JInvocation invoke = constructor.body().invoke("super");
        invoke.arg(JExpr.dotclass(jcm.ref(clazz)));
        return jdc;
    }

    private void createProperty(JCodeModel jcm, JDefinedClass jdc, Class clazz, PropertyDescriptor pd) throws
            JClassAlreadyExistsException {
        final String fieldName = pd.getName();
        System.out.println("Creating public static class for field " + fieldName + " on=" + jdc.fullName());
        try {
            JDefinedClass fieldClass = jdc._class(JMod.PUBLIC | JMod.STATIC, fieldName);
            fieldClass._extends(jcm.ref(MetaModelProperty.class));
            fieldClass.field(JMod.PUBLIC | JMod.STATIC | JMod.FINAL, fieldClass, "INSTANCE", JExpr._new(fieldClass));
            JMethod constructor = fieldClass.constructor(JMod.PUBLIC);
            JInvocation invoke = constructor.body().invoke("super");
            invoke.arg(JExpr.dotclass(jcm.ref(clazz)));
            invoke.arg(JExpr.dotclass(jcm._ref(pd.getPropertyType()).boxify()));
            invoke.arg(fieldName);
        } catch (Exception e) {
            log.error("Exception generating metamodel property", e);
            throw new RuntimeException(
                    "Exception generating metamodel property for " + pd.getName() + " on class" + jdc.name()
                    + " . This can happen when a field and its corresponding getter do not match in case like: user1product and the getter or setter being user1Product (with upper case P)",
                    e);
        }

        System.out.println("Creating public static class " + fieldName + " on=" + jdc.fullName() + " OK");
    }

    //Copied from https://code.google.com/p/eclipselink-staticweave-maven-plugin/source/browse/trunk/src/main/java/au/com/alderaan/eclipselink/mojo/EclipselinkStaticWeaveMojo.java
    @SuppressWarnings({"unchecked"})
    private URL[] buildClassPath() throws MalformedURLException {
        List<URL> urls = new ArrayList<URL>();
        Set<Artifact> artifacts = (Set<Artifact>)project.getArtifacts();
        for (Artifact a : artifacts) {
            log.debug("Adding artifact to classpath: " + a);
            urls.add(a.getFile().toURI().toURL());
        }
        return urls.toArray(new URL[urls.size()]);
    }
}
