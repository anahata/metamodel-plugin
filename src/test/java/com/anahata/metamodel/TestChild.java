package com.anahata.metamodel;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Getter
@Setter
public class TestChild {
    private String id;

    private String name;
}
