package com.anahata.metamodel;

import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import org.hibernate.validator.constraints.URL;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Getter
public class TestModel {
    private String id;

    @NotNull
    @Size(min = 1, max = 255)
    private String name;

    @URL
    private String website;
    
    private byte[] logo;
    
    private boolean active;
    
    @NotNull(groups = TestGroup.class, payload = TestPayload.class)
    private BigDecimal discountParts;
    
    private TestChild child;
    
    public String getCalculatedField() {
        return "Hello, " + name + "!";
    }
    
    public int amount(int base) {
        return base + 25;
    }
}
