package com.anahata.metamodel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.maven.project.MavenProject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import static org.junit.Assert.*;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@RunWith(PowerMockRunner.class)
public class MetaModelGeneratorTest {
    @Mock
    private MavenProject project;
    
    private File outputDir;
    
    @Before
    public void setUp() throws Exception {
        outputDir = createTempDir("JUnit");
    }
    
    @Test
    //ignoring since 1.1.0-SNAPSHOT as plugin context classloader seems to be different 
    //for test execution
    @Ignore
    public void testExecute() throws Exception {
        MetaModelGenerator metaModel = new MetaModelGenerator();
        System.out.println("Temp path: " + outputDir.getPath());
        Whitebox.setInternalState(metaModel, "outputDirectory", outputDir.getPath());
        Whitebox.setInternalState(metaModel, "project", project);
        List<String> classes = new ArrayList<>();
        classes.add(TestModel.class.getName());
        classes.add(TestChild.class.getName());
        Whitebox.setInternalState(metaModel, "classes", classes);
        metaModel.execute();
        getFile(TestModel.class);
        getFile(TestChild.class);
    }
    
    private File createTempDir(String prefix) throws IOException {
        File dir = File.createTempFile(prefix, "");
        dir.delete();
        dir.mkdir();
        return dir;
    }
    
    private void dumpFile(File file) throws IOException {
        List<String> lines = FileUtils.readLines(file);
        
        for (String line : lines) {
            System.out.println(line);
        }
    }
    
    private File getFile(Class clazz) throws IOException {
        final String filePath = clazz.getName().replace(".", "/") + "_mm.java";
        File file = new File(outputDir, filePath);
        System.out.println("Output file: " + file.getPath());
        assertTrue(file.exists());
        System.out.println("\nGenerated file:\n");
        dumpFile(file);
        return file;
    }
}
